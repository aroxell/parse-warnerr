# whiner

This little project will whine if two build logs are different, or parse the build log and create a json file.

## Usage

```
Usage: ./whine.py url-path-to-baselogfile [url-path-to-newlogfile]
./whine.sh url-pash-to-baselogfile	: prints a json file as output.
	   url-pash-to-newlogfile	: prints a diff file as output, between baselogfile and newlogfile.
	   --clang			: parse clang build.log files.
	   --remove-line-numbers	: drop linenumbers from the json list-buffer in the json file.
	   --sparse			: parse sparse build.log files.
```

## Example

How to run the example data:
```
./whine.py https://people.linaro.org/~anders.roxell/build-80.log
./whine.py --clang https://people.linaro.org/~anders.roxell/build-clang.log
./whine.py --sparse https://people.linaro.org/~anders.roxell/build-sparse.log
```
